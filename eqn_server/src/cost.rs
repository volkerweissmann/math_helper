use crate::ast_a::*;

pub const COST_LITERAL: i32 = 1;
pub const COST_SYMBOL: i32 = 4;
pub const COST_BRAKET: i32 = 10;
pub const COST_NORMAL_POWER: i32 = 2;
pub const COST_DIV: i32 = 2;
pub const COST_FUNC: i32 = 10;
pub const COST_SUMMAND: i32 = 1;

impl Number {
    pub fn cost_of_simplest(&self) -> i32 {
        match self {
            Number::Literal(_) => COST_LITERAL,
            Number::Symbol(_) => COST_SYMBOL,
        }
    }
}

impl Single {
    pub fn cost_of_simplest(&self) -> i32 {
        match self {
            Single::Number(x) => x.cost_of_simplest(),
            Single::Braket(s, _, _) => s.cost_of_simplest() + COST_BRAKET,
            Single::Power(bp) => match bp.2 {
                PowerFormat::Normal(_, _) => {
                    bp.0.cost_of_simplest() + bp.1.cost_of_simplest() + COST_NORMAL_POWER
                }
                PowerFormat::Div(_) => bp.0.cost_of_simplest() + COST_DIV,
            },
            Single::Function(f) => {
                COST_FUNC + f.1.iter().map(|x| x.cost_of_simplest()).sum::<i32>()
            }
            Single::InnerTex(_) => panic!(),
        }
    }
    fn get_base_and_exponent(&self) -> (&Single, Single) {
        match self {
            Single::Number(_) => (self, 1.into()),
            Single::Braket(_, _, _) => (self, 1.into()),
            Single::Power(bp) => (&bp.0, bp.1.clone()),
            Single::Function(_) => (self, 1.into()),
            Single::InnerTex(_) => panic!(),
        }
    }
}

impl Prod {
    /// Does this transformation: `fac d fac^b fac^c -> d fac^(1+b+c)`
    fn mul_to_exp_sum(self, fac: &Single) -> Self {
        if self.0.len() == 1 {
            // Without this short-circuit evaluation, we got infinite recursion
            return self;
        }
        let exponents: Vec<_> = self
            .0
            .iter()
            .filter_map(|x| -> Option<Prod> {
                let (base, exp) = x.get_base_and_exponent();
                if is_equal(base, fac) {
                    Some(exp.into())
                } else {
                    None
                }
            })
            .collect();
        let exponent = Sum::lazy_ws_plus(exponents).simplify();

        let mut factors: Vec<_> = self
            .0
            .into_iter()
            .filter(|x| -> bool {
                if let Some(bp) = x.as_power() {
                    !is_equal(&bp.0, fac)
                } else {
                    is_equal(x, fac)
                }
            })
            .collect();

        let exponent = if exponent.0.len() == 1 && exponent.0[0].0 .0.len() == 1 {
            exponent.0[0].0 .0[0].clone()
        } else {
            Single::Braket(exponent, LAZY_WS, LAZY_WS)
        };
        if exponent != 1.into() {
            factors.push(Single::Power(Box::new(Power(
                fac.clone(),
                exponent,
                PowerFormat::Normal(LAZY_WS, LAZY_WS),
            ))));
        } else {
            factors.push(fac.clone());
        }

        Prod::lazy_ws(factors)

        // todo: do something here
    }
    fn cost(&self) -> i32 {
        self.0.iter().map(|fac| fac.cost_of_simplest()).sum()
    }
    pub fn simplify(&self) -> Self {
        let mut current = self.clone();
        for fac in self.0.iter() {
            let (base, _exp) = fac.get_base_and_exponent();
            let new = current.clone().mul_to_exp_sum(base);
            if new.cost() < current.cost() {
                current = new;
            }
        }
        current
    }
    fn cost_of_simplest(&self) -> i32 {
        self.simplify().cost()
    }
}

// use std::hash::Hash;
// use std::hash::Hasher;
// fn calculate_hash<T: Hash>(t: &T) -> u64 {
//     let mut s = std::collections::hash_map::DefaultHasher::new();
//     t.hash(&mut s);
//     s.finish()
// }

impl Sum {
    fn cost(&self) -> i32 {
        self.0
            .iter()
            .map(|(prod, _sign)| prod.cost_of_simplest() + COST_SUMMAND)
            .sum()
    }
    pub fn simplify(self) -> Self {
        let mut ret = self.literal_addition();
        ret.0 = ret
            .0
            .iter()
            .map(|(prod, sign)| (prod.simplify(), *sign))
            .collect();
        ret
    }
    pub fn cost_of_simplest(&self) -> i32 {
        self.clone().simplify().cost() // todo: not the correct thing to do here
    }
}

#[cfg(test)]
mod tests {
    use crate::codegen_eqn::CodegenEqnConfig;
    use crate::pest_parse_eqn::string_to_ast;

    #[test]
    fn test_simplify() {
        let pairs = [
            ("a a / a", "a"),
            ("a a", "a^2"),
            ("a", "a"),
            ("a^2", "a^2"),
            ("a b^2", "a b^2"),
            //("a/b", "a/b"),
        ];
        for (input, output) in pairs {
            let old = string_to_ast(input).unwrap();
            let new = old.simplify();
            assert_eq!(
                new.to_eqn(&CodegenEqnConfig { redo_ws: false }).trim(),
                output.trim()
            );
        }
    }
}

// impl Sum {
//     pub fn cost(&self) -> i32 {
//         self.to_eqn(&CodegenEqnConfig { redo_ws: false })
//             .chars()
//             .filter(|c| !c.is_whitespace())
//             .count() as i32
//     }
//     pub fn remove_factor(&mut self, fact: &Single) {
//         self.0.iter_mut().for_each(|(prod, _sign)| {
//             prod.remove_factor(fact);
//         });
//     }
//     pub fn extract_factor_from_all_summands(mut self, fact: &Single) -> Prod {
//         self.remove_factor(fact);
//         Prod(
//             vec![
//                 fact.clone(),
//                 Single::Braket(self, Whitespace::default(), Whitespace::default()),
//             ],
//             vec![Whitespace::default()],
//         )
//     }
//     pub fn maybe_extract_factor(self) -> Self {
//         let candidates: Vec<Single> = self
//             .0
//             .iter()
//             .map(|(prod, _sign)| prod.0.clone())
//             .flatten()
//             .collect();
//         let mut current = self;
//         for candidate in candidates {
//             let new = current
//                 .clone()
//                 .extract_factor_from_some_summands(&candidate);
//             let old_cost = current.cost();
//             let new_cost = new.cost();
//             if new_cost < old_cost {
//                 current = new;
//             }
//             //current
//             // let mask: Vec<bool> = current
//             //     .0
//             //     .iter()
//             //     .map(|(prod, sign)| prod.0.contains(&el))
//             //     .collect();
//             // if mask.iter().filter(|b| **b).count() == 2 {}
//         }
//         current
//     }
// }
