use crate::ast_b::*;
use std::cell::RefCell;
use std::rc::Rc;

// todo: use static typing to prevent anyone from passing summands instead of factors
fn literal_multiplication(facs: Vec<Rc<RefCell<Inner>>>) -> Option<Replacement> {
    let len = facs.len();
    let (indexes, literals) = select(facs, |x| x.try_integer());

    // If there are zero ore one literal, we cannot multiply anthing
    // I commented it out, because it it correct, but unneccessary
    // if indexes.len() <= 1 {
    //     return None;
    // }
    let prod = literals.iter().product();

    Some(if prod == 0 {
        // a b 0 -> 0
        Replacement {
            remove: (0..len).collect(),
            additional: vec![Inner::Literal(0)],
        }
    } else if prod == 1 && indexes.len() < len {
        // the `indexes.len() < len` is there, because `1 -> 1` is correct, but `1 -> ` is not correct, you cannot have a product of 0 factors.
        // a b 1 -> a b
        Replacement {
            remove: indexes,
            additional: Vec::new(),
        }
    } else {
        // a b 2 3 -> a b 6
        Replacement {
            remove: indexes,
            additional: vec![Inner::Literal(prod)],
        }
    })
}

// todo: use static typing to prevent anyone from passing factors instead of summands
fn literal_addition(summands: Vec<Rc<RefCell<Inner>>>) -> Option<Replacement> {
    dbg!(&summands);
    let len = summands.len();
    let (indexes, literals) = select(summands, |x| x.try_integer());

    let sum: i32 = literals.iter().sum();

    Some(if sum == 0 && indexes.len() < len {
        // the `indexes.len() < len` is there, because `0 -> 0` is correct, but `0 -> ` is not correct, you cannot have a sum of 0 summands.
        // a + b + 0 -> a + b
        Replacement {
            remove: indexes,
            additional: Vec::new(),
        }
    } else {
        // a + b + 2 + 3 -> a + b + 5
        Replacement {
            remove: indexes,
            additional: vec![Inner::Literal(sum)],
        }
    })
}

#[allow(dead_code)]
pub fn simplify(node: Rc<RefCell<Inner>>) {
    flatten_single_arg(node.clone());
    let variant = node.borrow().get_variant();
    if variant == InnerVariant::Prod {
        apply_factor_trafo(node.clone(), literal_multiplication);
    } else if variant == InnerVariant::Sum {
        apply_summand_trafo(node.clone(), literal_addition);
    }

    match &*node.borrow() {
        Inner::Literal(_) => (),
        Inner::Variable(_) => (),
        Inner::Function(_, vec) => vec.iter().for_each(|el| simplify(Rc::clone(el))),
        Inner::Power(base, exponent) => {
            simplify(Rc::clone(base));
            simplify(Rc::clone(exponent));
        }
        Inner::Prod(vec) => {
            vec.iter().for_each(|el| simplify(Rc::clone(el)));
        }
        Inner::Sum(vec) => vec.iter().for_each(|el| simplify(Rc::clone(el))),
    }
}
