use crate::ast_a::*;
use pest::iterators::Pair;
use pest::iterators::Pairs;
use pest::Parser;

#[derive(Parser)]
#[grammar = "eqn.pest"]
struct EqnParser;

/// The type `Rule` gets defined by `#[derive(Parser)]` but has some weird visibility.
pub type ExportRule = Rule;
/// The function EqnParser::parse gets defined by `#[derive(Parser)]` but has some weird visibility.
pub fn export_eqn_parse(rule: Rule, s: &str) -> Result<Pairs<'_, Rule>, pest::error::Error<Rule>> {
    EqnParser::parse(rule, s)
}

/// Errors that don't happen inside pest's code, but in the code that parses
/// pest's output Called `TuringParserError` because rust is turing complete,
/// but the pest grammar description language is not
pub type TuringParserError = String;

//type ParseResult<T> = Result<T, TuringParserError>;

fn parse_func(pair: Pair<'_, Rule>) -> Result<Func, TuringParserError> {
    assert_eq!(pair.as_rule(), Rule::func);
    let mut it = pair.into_inner();
    let name = it.next().unwrap();
    assert!(name.as_rule() == Rule::symbol);
    let name = String::from(name.as_str());
    let mut spacing = vec![
        parse_whitespace(it.next().unwrap()),
        parse_whitespace(it.next().unwrap()),
    ];
    let mut args = vec![parse_sum(it.next().unwrap())?];
    spacing.push(parse_whitespace(it.next().unwrap()));
    while it.peek().is_some() {
        spacing.push(parse_whitespace(it.next().unwrap()));
        args.push(parse_sum(it.next().unwrap())?);
        spacing.push(parse_whitespace(it.next().unwrap()));
    }
    assert!(it.peek().is_none());
    Ok(Func(Symbol(name), args, spacing))
}

fn parse_inner_tex(pair: Pair<'_, Rule>) -> Result<&str, TuringParserError> {
    assert_eq!(pair.as_rule(), Rule::inner_tex);
    let str = pair.as_str();
    Ok(&str[4..str.len() - 1])
}

fn parse_single(pair: Pair<'_, Rule>) -> Result<Single, TuringParserError> {
    assert_eq!(pair.as_rule(), Rule::single);
    let mut single = pair.into_inner();
    let core = single.next().unwrap();
    let mut ret = match core.as_rule() {
        Rule::symbol => Single::Number(Number::Symbol(Symbol(String::from(core.as_str())))),
        Rule::integer => {
            let lit = core.as_str().parse();
            if lit.is_err() {
                return Err("integers larger than i32 are currently not supported".to_string());
            }
            let lit = lit.unwrap();
            if lit != 0 && core.as_str().starts_with('0') {
                return Err("integers with leading zeros are currently not supported".to_string());
            }
            Single::Number(Number::Literal(lit))
        }
        Rule::braket => {
            let mut core = core.into_inner();
            let w1 = parse_whitespace(core.next().unwrap());
            let sum = parse_sum(core.next().unwrap())?;
            let w2 = parse_whitespace(core.next().unwrap());
            assert!(core.peek().is_none());
            Single::Braket(sum, w1, w2)
        }
        Rule::func => Single::Function(parse_func(core)?),
        Rule::inner_tex => Single::InnerTex(parse_inner_tex(core)?.to_owned()),
        _ => panic!("unexpected rule: {:?}", core.as_rule()),
    };
    if single.peek().is_some() {
        let w1 = parse_whitespace(single.next().unwrap());
        assert_eq!(single.next().unwrap().as_rule(), Rule::power_sign);
        let w2 = parse_whitespace(single.next().unwrap());
        let exponent = single.next().unwrap();
        ret = Single::Power(Box::new(Power(
            ret,
            parse_single(exponent)?,
            PowerFormat::Normal(w1, w2),
        )));
    }
    assert!(single.peek().is_none());
    Ok(ret)
}

fn parse_prod(pair: Pair<'_, Rule>) -> Result<Prod, TuringParserError> {
    assert_eq!(pair.as_rule(), Rule::product);
    let mut prod = pair.into_inner();
    let mut count = 0;

    let mut factors = Vec::new();
    let mut spacing = Vec::new();
    while let Some(el) = prod.next() {
        match count % 2 {
            0 => {
                if el.as_rule() == Rule::div_sign {
                    let w = parse_whitespace(prod.next().unwrap());
                    factors.push(Single::Power(Box::new(Power(
                        parse_single(prod.next().unwrap())?,
                        Single::Number(Number::Literal(-1)),
                        PowerFormat::Div(w),
                    ))));
                } else {
                    factors.push(parse_single(el)?);
                };
            }
            1 => {
                spacing.push(parse_whitespace(el));
            }
            _ => unreachable!(),
        }
        count += 1;
    }
    assert_eq!(count % 2, 1);
    Ok(Prod(factors, spacing))
}

fn parse_sign(pair: Pair<'_, Rule>) -> Sign {
    match pair.as_rule() {
        Rule::sign => match pair.as_str() {
            "+" => Sign::ExplicitPlus,
            "-" => Sign::Minus,
            _ => panic!(),
        },
        Rule::optional_sign => match pair.as_str() {
            "" => Sign::ImplicitPlus,
            "+" => Sign::ExplicitPlus,
            "-" => Sign::Minus,
            _ => panic!(),
        },
        _ => panic!(),
    }
}

fn parse_whitespace(w: Pair<'_, Rule>) -> Whitespace {
    assert_eq!(w.as_rule(), Rule::w);
    Whitespace(String::from(w.as_str()))
}

fn parse_sum(pair: Pair<'_, Rule>) -> Result<Sum, TuringParserError> {
    assert_eq!(pair.as_rule(), Rule::sum);
    let mut count = 0;

    let mut sign = None;
    let mut summands = Vec::new();
    let mut spacing = Vec::new();
    for el in pair.into_inner() {
        match count % 4 {
            0 => {
                let sgn = parse_sign(el);
                assert!(count == 0 || sgn != Sign::ImplicitPlus);
                sign = Some(sgn);
            }
            1 => {
                spacing.push(parse_whitespace(el));
            }
            2 => {
                let prod = parse_prod(el)?;
                summands.push((prod, sign.take().unwrap()));
            }
            3 => {
                spacing.push(parse_whitespace(el));
            }
            _ => unreachable!(),
        }
        count += 1;
    }
    assert_eq!(count % 4, 3);
    Ok(Sum(summands, spacing))
}

pub fn parse_comparison_op(pair: Pair<'_, Rule>) -> Result<CmpOp, TuringParserError> {
    assert_eq!(pair.as_rule(), Rule::comparison_op);
    Ok(match pair.as_str() {
        "=" => CmpOp::Equal,
        ":=" => CmpOp::Definition,
        "<" => CmpOp::Lesser,
        "<=" => CmpOp::LesserEqual,
        ">" => CmpOp::Greater,
        ">=" => CmpOp::GreaterEqual,
        _ => panic!(),
    })
}

pub fn parse_comparison(pair: Pair<'_, Rule>) -> Result<Comparison, TuringParserError> {
    assert_eq!(pair.as_rule(), Rule::comparison);
    let mut comparison = pair.into_inner();
    let lhs = parse_sum(comparison.next().unwrap())?;
    assert_eq!(comparison.next().unwrap().as_rule(), Rule::w);
    let cmp_op = parse_comparison_op(comparison.next().unwrap())?;
    assert_eq!(comparison.next().unwrap().as_rule(), Rule::w);
    let rhs = parse_sum(comparison.next().unwrap())?;
    assert_eq!(comparison.next(), None);
    Ok(Comparison { cmp_op, lhs, rhs })
}

pub fn parse_total(mut total: pest::iterators::Pairs<'_, Rule>) -> Result<Sum, TuringParserError> {
    let export = total.next().unwrap();
    assert_eq!(export.as_rule(), Rule::export);
    let mut export = export.into_inner();
    let prefix_whitespace = export.next().unwrap();
    assert_eq!(prefix_whitespace.as_rule(), Rule::w);
    let sum = export.next().unwrap();
    let ret = parse_sum(sum);
    let suffix_whitespace = export.next().unwrap();
    assert_eq!(suffix_whitespace.as_rule(), Rule::w);
    assert_eq!(export.next().unwrap().as_rule(), Rule::EOI);
    assert_eq!(export.next(), None);
    assert_eq!(total.next(), None);
    ret
}

pub fn string_to_ast(str: &str) -> Result<Sum, TuringParserError> {
    let total = EqnParser::parse(Rule::export, str);
    let total = match total {
        Ok(v) => v,
        Err(e) => {
            return Err(format!("{}", e));
        }
    };
    parse_total(total)
}

#[cfg(test)]
pub mod tests {
    use super::*;

    // cargo fuzz run fuzz_target_1
    // cargo fuzz run fuzz_target_1 -- -only_ascii=1
    // cargo fuzz run fuzz_target_1 -- -help=1

    #[test]
    fn check_if_parsable() {
        let _pairs = EqnParser::parse(
            Rule::export,
            "partial_j[partial_k [ Q[j,k]e^(-phi[j,k])/epsilon ]]",
        )
        .unwrap_or_else(|e| panic!("{}", e));
    }
}
