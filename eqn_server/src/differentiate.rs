use crate::ast_a::*;
use crate::special;

impl Single {
    pub fn differentiate(&self, diffvar: &Symbol) -> Single {
        match self {
            Single::Number(x) => match x {
                Number::Literal(_) => Single::Number(Number::Literal(0)), // d/dx 17 = 0
                Number::Symbol(s) => {
                    if s == diffvar {
                        Single::Number(Number::Literal(1)) // d/dx x = 1
                    } else {
                        Single::Number(Number::Literal(0)) // d/dx y = 0
                    }
                }
            },
            Single::Braket(s, w1, w2) => {
                Single::Braket(s.differentiate(diffvar), w1.clone(), w2.clone())
            }
            Single::Power(bp) => {
                let f = &bp.0;
                let g = &bp.1;
                if f == &Symbol(special::SYM_EULER.to_string()).into() {
                    // d/dx e ^ g(x) = e ^ g(x) * g'(x)
                    let prod = Prod(
                        vec![bp.clone().into(), g.differentiate(diffvar)],
                        vec![Whitespace::default()],
                    );
                    Single::Braket(prod.into(), Whitespace::default(), Whitespace::default())
                } else {
                    // d/dx f(x) ^ g(x) = f(x)^(g(x) - 1) ( g(x) f'(x) + g'(x) f(x) ln f(x) ))
                    let prod = Prod(
                        vec![
                            Single::Power(Box::new(Power(
                                bp.0.clone(),
                                Single::Braket(
                                    Sum(
                                        vec![
                                            (Prod(vec![g.clone()], vec![]), Sign::ImplicitPlus),
                                            (Number::Literal(1).into(), Sign::Minus),
                                        ],
                                        vec![Whitespace::default(); 3],
                                    ),
                                    Whitespace::default(),
                                    Whitespace::default(),
                                ),
                                PowerFormat::Normal(Whitespace::default(), Whitespace::default()),
                            ))),
                            Single::Braket(
                                Sum(
                                    vec![
                                        (
                                            Prod(
                                                vec![f.differentiate(diffvar), g.clone()],
                                                vec![Whitespace::default()],
                                            ),
                                            Sign::ImplicitPlus,
                                        ),
                                        (
                                            Prod(
                                                vec![
                                                    f.clone(),
                                                    g.differentiate(diffvar),
                                                    Single::Function(Func(
                                                        Symbol(special::SYM_LN.to_string()),
                                                        vec![f.clone().into()],
                                                        vec![Whitespace::default(); 3],
                                                    )),
                                                ],
                                                vec![Whitespace::default(); 2],
                                            ),
                                            Sign::ExplicitPlus,
                                        ),
                                    ],
                                    vec![Whitespace::default(); 3],
                                ),
                                Whitespace::default(),
                                Whitespace::default(),
                            ),
                        ],
                        vec![Whitespace::default()],
                    );
                    Single::Braket(prod.into(), Whitespace::default(), Whitespace::default())
                }
            }
            Single::Function(f) => Single::Function(Func::differential(diffvar, f.clone().into())),
            Single::InnerTex(_) => 0.into(),
        }
    }
}
impl Prod {
    pub fn differentiate(&self, diffvar: &Symbol) -> Prod {
        let s = Sum(
            (0..self.0.len())
                .map(|i| {
                    let mut vec = self.0.clone();
                    vec[i] = vec[i].differentiate(diffvar); // d/dx f(x) g(x) = f'(x) g(x) + f(x) g'(x)
                    if i == 0 {
                        (Prod(vec, self.1.clone()), Sign::ImplicitPlus)
                    } else {
                        (Prod(vec, self.1.clone()), Sign::ExplicitPlus)
                    }
                })
                .collect(),
            vec![Whitespace::default(); self.0.len() * 2 - 1],
        );

        Prod(
            vec![Single::Braket(
                s,
                Whitespace::default(),
                Whitespace::default(),
            )],
            vec![],
        )
    }
}
impl Sum {
    pub fn differentiate(&self, diffvar: &Symbol) -> Sum {
        //self.0
        Sum(
            self.0
                .iter()
                .map(|(prod, sign)| (prod.differentiate(diffvar), *sign))
                .collect(),
            self.1.clone(),
        )
    }
}

impl Sum {
    pub fn expand_outer_command(&self) -> Sum {
        assert_eq!(self.0.len(), 1);
        let (prod, sign) = &self.0[0];
        assert!(*sign == Sign::ExplicitPlus || *sign == Sign::ImplicitPlus);
        assert_eq!(prod.0.len(), 1);
        let single = &prod.0[0];
        let f = single.as_function().unwrap();
        let diffvar = f.is_total_differential().unwrap();
        assert_eq!(f.1.len(), 1);
        let ret = f.1[0].differentiate(&diffvar);
        ret.sanity_check();
        let ret = ret.trafo(&crate::trivial::MY_TRAFOS);
        ret.sanity_check();
        ret
    }
}
