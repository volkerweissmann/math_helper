#!/bin/python
import random

vars = ["a", "b", "c", "d", "e"]

brak_open = " ( "
brak_close = " ) "


class Prod:
    def __init__(self, factors):
        self.factors = factors

    def __str__(self):
        ret = ""
        for el in self.factors:
            ret += el
        return ret

    def __repr__(self):
        str(self)


class Sum:
    def __init__(self, summands):
        self.summands = summands

    def __str__(self):
        ret = brak_open
        for el in self.summands:
            ret += " + " + str(el)
        ret += brak_close
        return ret

    def __repr__(self):
        str(self)


def random_prod():
    ret = []
    for el in vars:
        if random.randint(0, 10) < 6:
            ret.append(el)
    if len(ret) == 0:
        return random_prod()
    random.shuffle(ret)
    return Prod(ret)


def random_sum():
    ret = []
    for i in range(10):
        ret.append(random_prod())
    return Sum(ret)


print(random_sum())
