//! Converts our AST to latex
//! Note: f(x) could either be the product of f and x or the function f evaluated at x
//! Note: The output contains only very little whitespace

use crate::ast_a::*;
use crate::special::*;

#[derive(Default, Clone, Copy)]
pub struct SymConfig {
    pub bold: bool,
    pub hat: bool,
}

#[derive(Default)]
pub struct TexConfig {
    pub sym_confs: std::collections::HashMap<String, SymConfig>,
}

impl TexConfig {
    fn get_conf(&self, key: &str) -> SymConfig {
        match self.sym_confs.get(key) {
            Some(v) => *v,
            None => SymConfig::default(),
        }
    }
}

impl Sign {
    pub fn to_latex(&self, _conf: &TexConfig) -> &str {
        match self {
            Sign::Minus => "-",
            Sign::ExplicitPlus => "+",
            Sign::ImplicitPlus => "",
        }
    }
}

fn is_greek(x: &str) -> bool {
    for el in UNICODE_LONGFORMS {
        if x == el.1 {
            return true;
        }
    }
    false
}

/// This function is nearly the identity function
fn fix_syms(x: &str) -> String {
    if x.starts_with(DIFF_PREFIX) {
        format!("\\partial_{{{}}}", &fix_syms(&x[DIFF_PREFIX.len()..]))
    } else {
        x.split("_")
            .map(|s| {
                if is_greek(s) {
                    format!("\\{}", s)
                } else {
                    s.to_owned()
                }
            })
            .collect::<Vec<_>>()
            .join("_")
    }
}
fn maybe_bold_hat(var: &str, c: SymConfig) -> String {
    match (c.bold, c.hat) {
        (false, false) => format!("{} ", var),
        (true, false) => format!("\\bm{{{}}}", var),
        (false, true) => {
            let ind = var.find('_').unwrap_or_else(|| var.len());
            format!("\\hat{{{}}}{}", &var[..ind], &var[ind..])
        }
        (true, true) => {
            let ind = var.find('_').unwrap_or_else(|| var.len());
            format!("\\bm{{\\hat{{{}}}{}}}", &var[..ind], &var[ind..])
        }
    }
}
impl Number {
    pub fn to_latex(&self, conf: &TexConfig) -> String {
        match self {
            // todo if x is negative, we might need to return (-1)
            Number::Literal(x) => {
                if *x < 0 {
                    format!("({})", x)
                } else {
                    format!("{}", x)
                }
            }
            Number::Symbol(x) => {
                let c = conf.get_conf(&x.0);
                let var = fix_syms(&x.0);
                maybe_bold_hat(&var, c)
            }
        }
    }
}
impl Power {
    pub fn to_latex(&self, conf: &TexConfig) -> String {
        let exponent = match &self.1 {
            Single::Braket(s, _, _) => s.to_latex(conf),
            _ => self.1.to_latex(conf),
        };
        match &self.2 {
            PowerFormat::Normal(_, _) => {
                format!("{}^{{{}}} ", self.0.to_latex(conf), exponent)
            }
            PowerFormat::Div(_) => {
                debug_assert_eq!(self.1, (-1).into());
                format!("/{}", self.0.to_latex(conf))
            }
        }
    }
}
impl Func {
    pub fn to_latex(&self, conf: &TexConfig) -> String {
        if self.0 .0 == "sqrt" {
            return format!(
                "\\sqrt{{{}}}",
                self.1
                    .iter()
                    .map(|x| x.to_latex(conf))
                    .collect::<Vec<String>>()
                    .join(",")
            );
        }
        if TEX_FUNCS.iter().any(|s| *s == self.0 .0) {
            return format!(
                "\\{}\\left({}\\right) ",
                &self.0 .0,
                self.1
                    .iter()
                    .map(|x| x.to_latex(conf))
                    .collect::<Vec<String>>()
                    .join(",")
            );
        }
        format!(
            "{}\\left({}\\right) ",
            maybe_bold_hat(&fix_syms(&self.0 .0), conf.get_conf(&self.0 .0)),
            self.1
                .iter()
                .map(|x| x.to_latex(conf))
                .collect::<Vec<String>>()
                .join(",")
        )
    }
}
impl Single {
    pub fn to_latex(&self, conf: &TexConfig) -> String {
        match self {
            Single::Number(x) => x.to_latex(conf),
            Single::Braket(s, _, _) => format!("\\left({}\\right)", s.to_latex(conf)),
            Single::Power(bp) => bp.to_latex(conf),
            Single::Function(f) => f.to_latex(conf),
            Single::InnerTex(x) => x.into(),
        }
    }
}
impl Prod {
    pub fn to_latex(&self, conf: &TexConfig) -> String {
        self.0
            .iter()
            .map(|x| x.to_latex(conf))
            .collect::<Vec<String>>()
            .join(" ")
        //.join("\\cdot ")
    }
}
impl Sum {
    pub fn to_latex(&self, conf: &TexConfig) -> String {
        self.0
            .iter()
            .map(|x| format!("{}{}", x.1.to_latex(conf), x.0.to_latex(conf)))
            .collect::<Vec<String>>()
            .join("")
    }
}
impl Comparison {
    pub fn to_latex(&self, conf: &TexConfig) -> String {
        let cmp_op = match self.cmp_op {
            CmpOp::Equal => "&=",
            CmpOp::Definition => "&\\equiv",
            CmpOp::Lesser => "&<",
            CmpOp::LesserEqual => "&\\leq",
            CmpOp::Greater => "&>",
            CmpOp::GreaterEqual => "&\\geq",
        };
        format!(
            "{} {} {}",
            self.lhs.to_latex(conf),
            cmp_op,
            self.rhs.to_latex(conf)
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::pest_parse_eqn::string_to_ast;

    #[test]
    fn symbols() {
        let mut conf = TexConfig::default();
        conf.sym_confs.insert(
            "L_q".to_string(),
            SymConfig {
                bold: false,
                hat: true,
            },
        );
        conf.sym_confs.insert(
            "L".to_string(),
            SymConfig {
                bold: false,
                hat: true,
            },
        );
        dbg!(string_to_ast("L_q").unwrap().to_latex(&conf));
        dbg!(string_to_ast("L").unwrap().to_latex(&conf));
    }
}
