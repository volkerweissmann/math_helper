//! This is the inverse of pest_parse_eqn as can be verified by the eqn_to_ast_eqn.rs fuzz case

use crate::ast_a::*;
pub const EMPTY_STRING: &String = &String::new();

pub struct CodegenEqnConfig {
    pub redo_ws: bool, //ignores whitespace and add its own whitespace instead
}

impl Whitespace {
    pub fn to_eqn(&self, conf: &CodegenEqnConfig) -> &String {
        if conf.redo_ws {
            EMPTY_STRING
        } else {
            &self.0
        }
    }
}
impl Sign {
    pub fn to_eqn(self, _conf: &CodegenEqnConfig) -> &'static str {
        match self {
            Sign::Minus => "-",
            Sign::ExplicitPlus => "+",
            Sign::ImplicitPlus => "",
        }
    }
}
impl Number {
    pub fn to_eqn(&self, conf: &CodegenEqnConfig) -> String {
        if conf.redo_ws {
            match self {
                Number::Literal(x) => {
                    if *x < 0 {
                        format!("({})", x)
                    } else {
                        format!("{} ", x)
                    }
                }
                Number::Symbol(x) => format!("{} ", x.0),
            }
        } else {
            match self {
                Number::Literal(x) => {
                    if *x < 0 {
                        format!("({})", x)
                    } else {
                        format!("{}", x)
                    }
                }
                Number::Symbol(x) => x.0.to_owned(),
            }
        }
    }
}
impl Power {
    pub fn to_eqn(&self, conf: &CodegenEqnConfig) -> String {
        match &self.2 {
            PowerFormat::Normal(w1, w2) => {
                format!(
                    "{}{}^{}{}",
                    self.0.to_eqn(conf),
                    w1.to_eqn(conf),
                    w2.to_eqn(conf),
                    self.1.to_eqn(conf)
                )
            }
            PowerFormat::Div(w) => {
                format!("/{}{}", w.to_eqn(conf), self.0.to_eqn(conf))
            }
        }
    }
}
impl Func {
    pub fn to_eqn(&self, conf: &CodegenEqnConfig) -> String {
        let mut middle = String::new();
        for i in 1..self.1.len() {
            middle = middle
                + ","
                + self.2[2 * i + 1].to_eqn(conf)
                + &self.1[i].to_eqn(conf)
                + self.2[2 * i + 2].to_eqn(conf);
        }
        format!(
            "{}{}[{}{}{}{}]",
            self.0 .0,
            self.2[0].to_eqn(conf),
            self.2[1].to_eqn(conf),
            self.1[0].to_eqn(conf),
            self.2[2].to_eqn(conf),
            middle
        )
    }
}
impl Single {
    pub fn to_eqn(&self, conf: &CodegenEqnConfig) -> String {
        match self {
            Single::Number(x) => x.to_eqn(conf),
            Single::Braket(s, w1, w2) => {
                format!("({}{}{})", w1.to_eqn(conf), s.to_eqn(conf), w2.to_eqn(conf))
            }
            Single::Power(x) => x.to_eqn(conf),
            Single::Function(x) => x.to_eqn(conf),
            Single::InnerTex(x) => format!("tex[{}]", x),
        }
    }
}
impl Prod {
    pub fn to_eqn(&self, conf: &CodegenEqnConfig) -> String {
        let factors: Vec<String> = (0..self.0.len())
            .map(|i| {
                let temp = if i != 0 {
                    self.1[i - 1].to_eqn(conf).to_owned()
                } else {
                    String::new()
                };
                temp + &self.0[i].to_eqn(conf)
            })
            .collect();

        let lengths: Vec<usize> = factors.iter().map(|s| s.len()).collect();
        if lengths.iter().sum::<usize>() < 40 {
            factors.join("")
        } else {
            factors
                .into_iter()
                .map(|s| {
                    if s.len() < 10 {
                        s
                    } else {
                        format!("\n    {}", s.replace("\n", "\n    "))
                    }
                })
                .collect::<Vec<String>>()
                .join("")
                + "\n"
        }

        // let mut ret = self.0[0].to_eqn(conf);
        // for i in 1..self.0.len() {
        //     ret = ret + self.1[i - 1].to_eqn(conf) + &self.0[i].to_eqn(conf);
        // }
        // ret
    }
}
impl Sum {
    pub fn to_eqn(&self, conf: &CodegenEqnConfig) -> String {
        let summands: Vec<String> = (0..self.0.len())
            .map(|i| {
                let temp = if i != 0 {
                    self.1[2 * i - 1].to_eqn(conf).to_owned()
                } else {
                    String::new()
                };
                temp + self.0[i].1.to_eqn(conf)
                    + self.1[2 * i].to_eqn(conf)
                    + &self.0[i].0.to_eqn(conf)
            })
            .collect();
        let lengths: Vec<usize> = summands.iter().map(|s| s.len()).collect();
        if lengths.iter().sum::<usize>() < 40 {
            summands.join("")
        } else {
            summands
                .into_iter()
                .map(|s| {
                    if s.len() < 10 {
                        s
                    } else {
                        format!("\n    {}", s.replace("\n", "\n    "))
                    }
                })
                .collect::<Vec<String>>()
                .join("")
                + "\n"
        }
    }
}

// impl Term {
//     pub fn to_eqn(&self) -> String {
//         format!("{}{}{}", self.1.to_eqn(conf), self.0.to_eqn(conf), self.2.to_eqn(conf))
//     }
// }
