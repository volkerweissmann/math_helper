//! Conversions between different ast's

use crate::ast_a;
use crate::ast_a::LAZY_WS;
use crate::ast_b;

use std::cell::RefCell;
use std::rc::Rc;

impl From<ast_a::Single> for ast_b::Inner {
    fn from(x: ast_a::Single) -> Self {
        match x {
            ast_a::Single::Number(ast_a::Number::Literal(x)) => ast_b::Inner::Literal(x),
            ast_a::Single::Number(ast_a::Number::Symbol(x)) => ast_b::Inner::Variable(x.0),
            ast_a::Single::Braket(x, _, _) => x.into(),
            ast_a::Single::Power(bp) => ast_b::Inner::Power(
                Rc::new(RefCell::new(bp.0.into())),
                Rc::new(RefCell::new(bp.1.into())),
            ),
            ast_a::Single::Function(f) => ast_b::Inner::Function(
                f.0 .0,
                f.1.into_iter()
                    .map(|x| Rc::new(RefCell::new(x.into())))
                    .collect(),
            ),
            ast_a::Single::InnerTex(_) => panic!(),
        }
    }
}
impl From<ast_a::Prod> for ast_b::Inner {
    fn from(x: ast_a::Prod) -> Self {
        let vec: Vec<_> =
            x.0.into_iter()
                .map(|fac| Rc::new(RefCell::new(fac.into())))
                .collect();
        ast_b::Inner::Prod(vec)
    }
}
impl From<ast_a::Sum> for ast_b::Inner {
    fn from(x: ast_a::Sum) -> Self {
        let vec: Vec<_> =
            x.0.into_iter()
                .map(|(mut prod, sign)| -> Rc<RefCell<ast_b::Inner>> {
                    match sign {
                        ast_a::Sign::ExplicitPlus | ast_a::Sign::ImplicitPlus => {
                            Rc::new(RefCell::new(prod.into()))
                        }
                        ast_a::Sign::Minus => {
                            prod.0.push((-1_i32).into());
                            Rc::new(RefCell::new(prod.into()))
                        }
                    }
                })
                .collect();
        ast_b::Inner::Sum(vec)
    }
}

impl From<ast_b::Inner> for ast_a::Single {
    fn from(x: ast_b::Inner) -> Self {
        match x {
            ast_b::Inner::Literal(i) => i.into(),
            ast_b::Inner::Variable(x) => ast_a::Symbol(x).into(),
            ast_b::Inner::Function(f, args) => ast_a::Func::lazy_ws(
                ast_a::Symbol(f),
                args.into_iter()
                    .map(|x| Rc::try_unwrap(x).unwrap().into_inner().into())
                    .collect(),
            )
            .into(),
            ast_b::Inner::Power(base, exp) => ast_a::Power(
                Rc::try_unwrap(base).unwrap().into_inner().into(),
                Rc::try_unwrap(exp).unwrap().into_inner().into(),
                ast_a::PowerFormat::Normal(LAZY_WS, LAZY_WS),
            )
            .into(),
            ast_b::Inner::Sum(_) => ast_a::Single::Braket(x.into(), LAZY_WS, LAZY_WS),
            ast_b::Inner::Prod(_) => panic!(),
        }
    }
}
impl From<ast_b::Inner> for ast_a::Prod {
    fn from(x: ast_b::Inner) -> Self {
        let vec = x
            .into_prod()
            .unwrap()
            .into_iter()
            .map(|x: Rc<RefCell<ast_b::Inner>>| -> ast_a::Single {
                Rc::try_unwrap(x).unwrap().into_inner().into()
            })
            .collect();
        ast_a::Prod::lazy_ws(vec)
    }
}
impl From<ast_b::Inner> for ast_a::Sum {
    fn from(x: ast_b::Inner) -> Self {
        let vec = x
            .into_sum()
            .unwrap()
            .into_iter()
            .map(|x: Rc<RefCell<ast_b::Inner>>| -> ast_a::Prod {
                let inner = Rc::try_unwrap(x).unwrap().into_inner();
                if matches!(&inner, ast_b::Inner::Prod(_)) {
                    inner.into()
                } else {
                    ast_b::Inner::Prod(vec![Rc::new(RefCell::new(inner))]).into()
                }
                // match inner {
                //     _ => ast_b::Inner::Prod(vec).into(),
                //     ast_b::Inner::Prod(vec) => (ast_b::Inner::Prod(vec).into()),
                // }
            })
            .collect();
        ast_a::Sum::lazy_ws_plus(vec)
    }
}
