use crate::ast_a::*;

impl Prod {
    pub fn remove_factor(&mut self, fact: &Single) {
        let index = self.0.iter().position(|x| *x == *fact).unwrap();
        self.0.remove(index);
        // let temp = format!("{}{}", &self.1[2 * index].0, &self.1[2 * index + 1].0);
        // self.1[2 * index] = Whitespace(temp);
        // self.1.remove(2 * index + 1);
        if self.0.is_empty() {
            self.0.push(1.into());
        }
        self.1 = vec![LAZY_WS; self.0.len() - 1];
    }
}

impl Sum {
    /// `a + 2 + 3 -> a + 5` and `a + 2 - 2 -> a`
    pub fn literal_addition(self) -> Self {
        let sum: i32 = self
            .0
            .iter()
            .map(|(prod, sign)| -> i32 {
                if prod.0.len() == 1 {
                    if let Single::Number(Number::Literal(x)) = &prod.0[0] {
                        return match sign {
                            Sign::ImplicitPlus => *x,
                            Sign::ExplicitPlus => *x,
                            Sign::Minus => -(*x),
                        };
                    }
                }
                0
            })
            .sum();

        let mut vec: Vec<_> = self
            .0
            .into_iter()
            .filter(|(prod, _sign)| {
                if prod.0.len() == 1 {
                    if let Single::Number(Number::Literal(_)) = &prod.0[0] {
                        return false;
                    }
                }
                true
            })
            .collect();
        if sum != 0 {
            vec.push((sum.into(), Sign::ExplicitPlus));
        }
        Sum::lazy_ws(vec)
    }
    pub fn extract_factor_from_some_summands(self, fact: &Single) -> Sum {
        let mask: Vec<bool> = self
            .0
            .iter()
            .map(|(prod, _sign)| prod.0.contains(fact))
            .collect();
        let mut summands = Vec::new();
        let mut changed = Vec::new();
        for (mut el, extract) in self.0.into_iter().zip(mask.iter()) {
            if *extract {
                el.0.remove_factor(fact);
                changed.push(el);
            } else {
                summands.push(el);
            }
        }
        if !changed.is_empty() {
            summands.push((
                Prod(
                    vec![
                        fact.clone(),
                        Single::Braket(
                            Sum::lazy_ws(changed),
                            Whitespace::default(),
                            Whitespace::default(),
                        ),
                    ],
                    vec![Whitespace::default()],
                ),
                Sign::ExplicitPlus,
            ));
        }
        Sum::lazy_ws(summands)
    }
}
