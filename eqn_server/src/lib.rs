#![feature(default_free_fn)]
//#![feature(let_chains)]

extern crate pest;
#[macro_use]
extern crate pest_derive;
extern crate enum_as_inner;
extern crate notify;

pub mod ast_a;
mod ast_b;
pub mod codegen_eqn;
pub mod codegen_latex;
mod conversions;
mod cost;
mod differentiate;
pub mod embedding;
mod glue;
pub mod pest_parse_eqn;
mod simple;
mod special;
mod trafos_b;
mod transformations;
mod trivial;
mod whitespace;

#[cfg(test)]
mod tests {
    use std::cell::RefCell;
    use std::fs::read_to_string;
    use std::rc::Rc;

    use super::*;
    use crate::codegen_eqn::*;
    use crate::codegen_latex::{SymConfig, TexConfig};
    use crate::pest_parse_eqn::string_to_ast;
    use crate::trafos_b::simplify;
    use crate::trivial::MY_TRAFOS;
    use notify::{watcher, RecursiveMode, Watcher};
    use std::sync::mpsc::channel;
    use std::time::Duration;

    #[test]
    fn oldwork() {
        // cargo test --lib -- curwork --nocapture

        // let term = string_to_ast("D_k [ Q[j,k]e^(-phi[j,k]/epsilon) ]").unwrap();
        // let expanded = term.expand_outer_command();
        // println!("{}", term.to_eqn(&CodegenEqnConfig { redo_ws: true }));
        // println!("\n{}", expanded.to_eqn(&CodegenEqnConfig { redo_ws: true }));
        // let double_expanded = string_to_ast(&format!(
        //     "D_j [{}]",
        //     expanded.to_eqn(&CodegenEqnConfig { redo_ws: true })
        // ))
        // .unwrap()
        // .expand_outer_command()
        // .trafo(&MY_TRAFOS);

        let double_expanded = string_to_ast("a  + 3 + 4 + 1 + b").unwrap();

        let double_expanded: ast_b::Inner = double_expanded.into();
        let double_expanded = Rc::new(RefCell::new(double_expanded));
        simplify(double_expanded.clone());
        let double_expanded: ast_a::Sum =
            Rc::try_unwrap(double_expanded).unwrap().into_inner().into();
        let double_expanded = double_expanded.trafo(&MY_TRAFOS);
        println!(
            "\n{}",
            double_expanded.to_eqn(&CodegenEqnConfig { redo_ws: true })
        );
    }

    #[test]
    fn curwork() {
        let mut conf = TexConfig::default();
        conf.sym_confs.insert(
            "q".to_string(),
            SymConfig {
                bold: true,
                hat: false,
            },
        );
        conf.sym_confs.insert(
            "L_q".to_string(),
            SymConfig {
                bold: false,
                hat: true,
            },
        );
        conf.sym_confs.insert(
            "L_x".to_string(),
            SymConfig {
                bold: false,
                hat: true,
            },
        );
        let input =
            read_to_string("/home/volker/Sync/DatenVolker/git/Masterarbeit/integral.tex").unwrap();
        let output = match embedding::latex_with_eqn_to_latex(&input, &conf) {
            Ok(v) => v,
            Err(e) => panic!("{}", e),
        };
        std::fs::write(
            "/home/volker/Sync/DatenVolker/git/Masterarbeit/integral.myout",
            output,
        )
        .expect("unable to write");
    }

    #[test]
    //#[ignore]
    fn prototype_build() {
        // Create a channel to receive the events.
        let (tx, rx) = channel();

        // Create a watcher object, delivering debounced events.
        // The notification back-end is selected based on the platform.
        let mut watcher = watcher(tx, Duration::from_secs(1)).unwrap();

        // Add a path to be watched. All files and directories at that path and
        // below will be monitored for changes.
        watcher
            .watch(
                "/home/volker/Sync/DatenVolker/git/Masterarbeit/integral.tex",
                RecursiveMode::Recursive,
            )
            .unwrap();

        let conf = TexConfig::default();

        loop {
            match rx.recv() {
                Ok(event) => match event {
                    notify::DebouncedEvent::NoticeWrite(_) => {
                        dbg!(&event);
                        std::thread::sleep(std::time::Duration::from_millis(100));
                        let input = read_to_string(
                            "/home/volker/Sync/DatenVolker/git/Masterarbeit/integral.tex",
                        )
                        .unwrap();

                        dbg!(&input.len());
                        let output = embedding::latex_with_eqn_to_latex(&input, &conf);
                        if let Ok(output) = output {
                            std::fs::write(
                                "/home/volker/Sync/DatenVolker/git/Masterarbeit/integral.myout",
                                output,
                            )
                            .expect("unable to write");
                        } else {
                            dbg!(output.unwrap_err());
                        }
                    }
                    _ => (),
                },
                Err(e) => println!("watch error: {:?}", e),
            }
        }
    }
}
