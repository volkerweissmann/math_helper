use crate::codegen_latex::TexConfig;
use crate::pest_parse_eqn::{export_eqn_parse, parse_comparison, ExportRule, TuringParserError};

#[allow(dead_code)]
pub fn latex_with_eqn_to_latex(
    latex_with_eqn: &str,
    conf: &TexConfig,
) -> Result<String, TuringParserError> {
    let total = export_eqn_parse(ExportRule::embedded_in_latex, latex_with_eqn);
    let mut total = match total {
        Ok(v) => v,
        Err(e) => {
            return Err(format!("{}", e));
        }
    };
    let embedded_in_latex = total.next().unwrap();
    assert_eq!(embedded_in_latex.as_rule(), ExportRule::embedded_in_latex);
    let embedded_in_latex = embedded_in_latex.into_inner();

    let mut ret = String::new();

    let mut flag = false;
    for el in embedded_in_latex {
        assert!(!flag);
        if el.as_rule() == ExportRule::EOI {
            flag = true;
            continue;
        }
        match el.as_rule() {
            ExportRule::eqn_embedded => {
                let mut eqn_embedded = el.into_inner();
                // let eq = parse_comparison(eqn_embedded.next().unwrap())?;
                // ret = format!("{}\\eq{{{}}}", ret, eq.to_latex(conf));
                let mut eqs = Vec::new();
                while eqn_embedded.peek().is_some() {
                    assert_eq!(eqn_embedded.next().unwrap().as_rule(), ExportRule::w);
                    eqs.push(parse_comparison(eqn_embedded.next().unwrap())?);
                    assert_eq!(eqn_embedded.next().unwrap().as_rule(), ExportRule::w);
                }
                let tot = eqs
                    .iter()
                    .map(|eq| eq.to_latex(conf))
                    .collect::<Vec<String>>()
                    .join(" \\\\ ");
                ret = format!("{}\\eq{{{}}}", ret, tot);
            }
            ExportRule::ignored_latex => {
                ret += el.as_str();
            }
            _ => panic!(),
        };
    }
    assert!(flag);
    assert_eq!(total.next(), None);
    Ok(ret)
}
