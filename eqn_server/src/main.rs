#![feature(bench_black_box)]

extern crate lazy_static;
extern crate regex;
extern crate serde_json;

use math_helper::codegen_eqn::*;
use serde::{Deserialize, Serialize};
use std::io::Write;
use std::io::{BufRead, BufReader};
use std::net::{TcpListener, TcpStream};

use math_helper::codegen_latex::TexConfig;

#[derive(Debug, Serialize, Deserialize)]
struct Input {
    cmd: String,
    start_line: usize,
    start_column: usize,
    end_line: usize,
    end_column: usize,
    editor_text: String,
}

fn get_pos(text: &str, line: usize, column: usize) -> usize {
    column
        + text
            .lines()
            .take(line)
            .map(|s| s.len() + 1) // the +1 is there because .lines() removes the "\n"'s at the end
            .sum::<usize>()
}

fn handle_client(mut stream: TcpStream) -> Result<(), ()> {
    let mut data = String::new();
    let mut reader = BufReader::new(&stream);
    let bytes_read = reader.read_line(&mut data).unwrap();
    if bytes_read == 0 {
        return Ok(());
    }
    let input: Input = serde_json::from_str(&data).unwrap();
    dbg!(&input);
    let start_pos = get_pos(&input.editor_text, input.start_line, input.start_column);
    let end_pos = get_pos(&input.editor_text, input.end_line, input.end_column);
    assert!(end_pos >= start_pos);
    let cursor_selected = &input.editor_text[start_pos..end_pos];
    dbg!(cursor_selected);
    if input.cmd == "latexToClipboard" {
        let ast = math_helper::pest_parse_eqn::string_to_ast(cursor_selected);
        let msg = match ast {
            Ok(v) => v.to_latex(&TexConfig::default()),
            Err(e) => e,
        };
        stream.write_all(msg.as_bytes()).unwrap();
        //cursor_selected
    } else if input.cmd == "expandOuterFunction" {
        let ast = math_helper::pest_parse_eqn::string_to_ast(cursor_selected);
        let msg = match ast {
            Ok(v) => v
                .expand_outer_command()
                .to_eqn(&CodegenEqnConfig { redo_ws: true }),
            Err(e) => e,
        };
        stream.write_all(msg.as_bytes()).unwrap();
        //cursor_selected
    } else {
        stream
            .write_all(b"command unknwon the the eqn_server")
            .unwrap();
    }
    Ok(())
}

fn main() -> std::io::Result<()> {
    let listener = TcpListener::bind("127.0.0.1:65432")?;
    // accept connections and process them serially
    for stream in listener.incoming() {
        handle_client(stream?).unwrap();
    }
    Ok(())
}
