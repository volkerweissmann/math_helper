# Simple Transformations

## Simple Simplifications
We are interested in a simplification routine, which understands the following 4 things.

a(b+c) = ab + ac
(bc)^a = b^a c^a
a^(b+c) = a^b a^c
Integer Addition and Multiplikation

We need to understand integer addition and multiplikation, to see stuff like
a^(2) a^(-1) = a

## The Problem

Let's say we have an expression that we want to simplify. Some transformations preserve equality and make the expression obviously simpler, like a + 0 -> a.
But some transformations preserve equality but it is up to debate which form is simpler, e.g. a+b -> b+a or a(b+c)+d -> ab+ac+d.
We need some way to choose wether or not to do such a transformation.


## The Solution: The cost function

We introduce a cost function, which is a function that maps every expression to an integer and says how "bad"/"complex" a given expression is. We do a transformation if it lowers the cost.


## Hybrid and Flattened Form

Our AST looks like this:

expr
    - summand
      - factor
        - base
        - exponent
      - factor
        - base
        - exponent
    - summand
      - factor
        - base
        - exponent
      - factor
        - base
        - exponent

Generally, base and exponent are expr's.

If base is an atomic (a.k.a a variable an integer or a function) and no two factors are the same, this is called the hybrid form.
There are no brakets (except for functions and around exponents) in the hybrid form.
When I say that an expression is in a hybrid form, I mean that the expression and all function arguments and exponents are in the hybrid form.

If base is an atomic (a.k.a a variable an integer or a function) and every exponent only has one summand, this is called the flattened form.
There are no brakets (except for functions and around exponents) in the flattened form.
When I say that an expression is in a hybrid form, I mean that the expression and all function arguments and exponents are in the flattened form.
In the flattened, form, we have a^2 instead of a a.

## Simple simplifications of the flattened form

Let us say, we have a flattened form, how could we apply the simple simplifications?
Paraphrasing, more pseudo-code than actual code
```rust
type Sum = Vec<Prod>;
type Prod = Vec<Power>;
struct Power{
    base: Atomic,
    exponent: Prod,
};
```

## Simplified Problem

Let's say we have only addition, multiplication and Symbols, the only allowed transformation is `a(b+c)=ab+bc`.

## Comparisons

If we are in the flattened form, we are on the rhs of these 3 equations

a(b+c) = ab + ac
(bc)^a = b^a c^a
a^(b+c) = a^b a^c

How do we check if the left side is better?

## Example Cases

bf + bae + ac + ad
should end up as
bf + a(be + c + d)
and not as
b(f + ae) + a(c + d)
