use crate::ast_a::*;

const NO_WHITE: Whitespace = Whitespace(String::new());
impl Sign {
    fn remove_whitespace(&self) -> Self {
        *self
    }
}
impl Symbol {
    fn remove_whitespace(&self) -> Self {
        self.clone()
    }
}
impl Number {
    fn remove_whitespace(&self) -> Self {
        self.clone()
    }
}
impl PowerFormat {
    fn remove_whitespace(&self) -> Self {
        match self {
            PowerFormat::Normal(_, _) => PowerFormat::Normal(NO_WHITE, NO_WHITE),
            PowerFormat::Div(_) => PowerFormat::Div(NO_WHITE),
        }
    }
}
impl Single {
    fn remove_whitespace(&self) -> Self {
        match self {
            Single::Number(x) => x.remove_whitespace().into(),
            Single::Braket(s, _, _) => Single::Braket(s.remove_whitespace(), NO_WHITE, NO_WHITE),
            Single::Power(bp) => Power(
                bp.0.remove_whitespace(),
                bp.1.remove_whitespace(),
                bp.2.remove_whitespace(),
            )
            .into(),
            Single::Function(f) => Func(
                f.0.remove_whitespace(),
                f.1.iter().map(|x| x.remove_whitespace()).collect(),
                f.2.iter().map(|_| NO_WHITE).collect(),
            )
            .into(),
            Single::InnerTex(_) => self.clone(),
        }
    }
}
impl Prod {
    fn remove_whitespace(&self) -> Self {
        Prod(
            self.0.iter().map(|x| x.remove_whitespace()).collect(),
            vec![NO_WHITE; self.0.len() - 1],
        )
    }
}
impl Sum {
    #[allow(dead_code)]
    fn remove_whitespace(&self) -> Self {
        Sum(
            self.0
                .iter()
                .map(|x| (x.0.remove_whitespace(), x.1.remove_whitespace()))
                .collect(),
            vec![NO_WHITE; self.0.len() * 2 - 1],
        )
    }
}

// impl Single {
//     pub fn redo_whitespace(&self, indent: i32) -> Self {
//         match self {
//             Single::Number(x) => x.remove_whitespace().into(),
//             Single::Braket(s, _, _) => {
//                 Single::Braket(s.redo_whitespace(indent), NO_WHITE, NO_WHITE)
//             }
//             Single::Power(bp) => Power(
//                 bp.0.remove_whitespace(),
//                 bp.1.remove_whitespace(),
//                 bp.2.remove_whitespace(),
//             )
//             .into(),
//             Single::Function(f) => Func(
//                 f.0.remove_whitespace(),
//                 f.1.iter().map(|x| x.remove_whitespace()).collect(),
//                 f.2.iter().map(|x| NO_WHITE).collect(),
//             )
//             .into(),
//         }
//     }
// }

// impl Prod {
//     pub fn redo_whitespace(&self, indent: i32) -> Self {
//         let no_ws_lens: Vec<usize> = self
//             .0
//             .iter()
//             .map(|x| x.redo_ws().chars().filter(|c| !c.is_whitespace()).count())
//             .collect();
//         if no_ws_lens.iter().sum::<usize>() < 40 {
//             self.remove_whitespace()
//         } else {
//             //let line_prefix = "    ".repeat(indent)
//             Prod(
//                 self.0
//                     .iter()
//                     .zip(no_ws_lens.iter())
//                     .map(|(x, len)| x.redo_whitespace(indent))
//                     .collect(),
//                 vec![NO_WHITE; self.0.len() - 1],
//             )
//         }
//     }
// }

// impl Sum {
//     pub fn redo_whitespace(&self, indent: i32) -> Self {
//         let no_ws_lens: Vec<usize> = self
//             .0
//             .iter()
//             .map(|x| {
//                 x.0.redo_ws().chars().filter(|c| !c.is_whitespace()).count()
//                     + x.1.redo_ws().chars().filter(|c| !c.is_whitespace()).count()
//             })
//             .collect();
//         if no_ws_lens.iter().sum::<usize>() < 40 {
//             self.remove_whitespace()
//         } else {
//             //let line_prefix = "    ".repeat(indent)
//             let summands = Vec::new();
//             let spacing = Vec::new();
//             self.0
//                     .iter()
//                     .zip(no_ws_lens.iter())
//                     .for_each(|(x, &len)| {
//                         if len < 10 {
//                             (x.0.redo_whitespace(indent), x.1.remove_whitespace())
//                         } else {
//                             (x.0.redo_whitespace(indent), x.1.remove_whitespace())
//                         }
//                     })
//                     .collect(),
//             Sum(
//                 summands,
//                 spacing,
//             )
//         }
//     }
// }

// impl Sign {
//     pub fn redo_ws(self) -> &'static str {
//         match self {
//             Sign::Minus => "-",
//             Sign::ExplicitPlus => "+",
//             Sign::ImplicitPlus => "",
//         }
//     }
// }
// impl Number {
//     pub fn redo_ws(&self) -> String {
//         match self {
//             Number::Literal(x) => {
//                 if *x < 0 {
//                     format!("({})", x)
//                 } else {
//                     format!("{}", x)
//                 }
//             }
//             Number::Symbol(x) => x.0.to_owned(),
//         }
//     }
// }
// impl Power {
//     pub fn redo_ws(&self) -> String {
//         match &self.2 {
//             PowerFormat::Normal(w1, w2) => {
//                 format!("{}^{}", self.0.redo_ws(), self.1.redo_ws())
//             }
//             PowerFormat::Div(w) => {
//                 format!("/{}", self.0.redo_ws())
//             }
//         }
//     }
// }
// impl Func {
//     pub fn redo_ws(&self) -> String {
//         let mut middle = String::new();
//         for i in 1..self.1.len() {
//             middle = middle + "," + &self.1[i].redo_ws()
//         }
//         format!("{}[{}]", self.0 .0, middle)
//     }
// }
// impl Single {
//     pub fn redo_ws(&self) -> String {
//         match self {
//             Single::Number(x) => x.redo_ws(),
//             Single::Braket(s, w1, w2) => {
//                 format!(" ({}) ", s.redo_ws())
//             }
//             Single::Power(x) => x.redo_ws(),
//             Single::Function(x) => x.redo_ws(),
//         }
//     }
// }
// impl Prod {
//     pub fn redo_ws(&self) -> String {
//         let mut ret = self.0[0].redo_ws();
//         for i in 1..self.0.len() {
//             ret = ret + &self.0[i].redo_ws();
//         }
//         ret
//     }
// }
// impl Sum {
//     pub fn redo_ws(&self) -> String {
//         let mut ret = String::new();
//         for i in 0..self.0.len() {
//             if i != 0 {
//                 ret = ret;
//             }
//             ret = ret + self.0[i].1.redo_ws() + &self.0[i].0.redo_ws();
//         }
//         ret
//     }
// }
