use crate::ast_a::*;

pub const SYM_LN: &str = "ln";
pub const SYM_EULER: &str = "e";
pub const DIFF_PREFIX: &str = "D_";
// todo: Some of these greek letters look nearly exactly like another letter (at least in my font)
pub const UNICODE_LONGFORMS: [(&str, &str); 41] = [
    ("α", "alpha"),
    ("β", "beta"),
    ("γ", "gamma"),
    ("Γ", "Gamma"),
    ("δ", "delta"),
    ("Δ", "Delta"),
    ("ϵ", "epsilon"),
    ("ε", "varepsilon"),
    ("ζ", "zeta"),
    ("η", "eta"),
    ("θ", "theta"),
    ("ϑ", "vartheta"),
    ("Θ", "theta"),
    ("ι", "iota"),
    ("κ", "kappa"),
    ("ϰ", "varkappa"),
    ("λ", "lambda"),
    ("Λ", "Lambda"),
    ("μ", "mu"),
    ("ν", "nu"),
    ("ξ", "xi"),
    ("Ξ", "Xi"),
    ("π", "pi"),
    ("ϖ", "varpi"),
    ("Π", "Pi"),
    ("ρ", "rho"),
    ("ϱ", "varrho"),
    ("σ", "sigma"),
    ("ς", "varsigma"),
    ("Σ", "Sigma"),
    ("τ", "tau"),
    ("υ", "upsilon"),
    ("φ", "phi"),
    ("ϕ", "varphi"),
    ("Φ", "Phi"),
    ("χ", "chi"),
    ("X", "Chi"),
    ("ψ", "psi"),
    ("Ψ", "Psi"),
    ("ω", "omega"),
    ("Ω", "Omega"),
];

pub const TEX_FUNCS: [&str; 2] = ["sin", "cos"];

impl Func {
    pub fn is_total_differential(&self) -> Option<Symbol> {
        Some(Symbol(self.0 .0.strip_prefix(DIFF_PREFIX)?.to_string()))
    }
    pub fn differential(diffvar: &Symbol, sum: Sum) -> Func {
        Func(
            Symbol(format!("{}{}", DIFF_PREFIX, diffvar.0)),
            vec![sum],
            vec![Whitespace::default(); 3],
        )
    }
    pub fn try_easy_eval(&self) -> Single {
        if self.0 .0 == SYM_LN {
            assert!(self.1.len() == 1);
            if self.1[0] == 1.into() {
                return 0.into();
            } else if self.1[0] == Symbol(SYM_EULER.to_string()).into() {
                return 1.into();
            }
        }
        self.clone().into()
    }
}
