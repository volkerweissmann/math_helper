//! Does trivial transformations of the ast
//! Transformations considered trivial (non-exhaustive):
//!  -  `a + 0` to `a`
//!  -  `a * 1` to `a`
//!  -  `a ^ 1` to `a`
//!  -  `(a)` to `a`
//! Transformations considered not trivial (non-exhaustive):
//!  -  `a + b` to `b + a`
//!  -  `1 + 1` to `2`
//!  -  `+ a` to `a`

use crate::{ast_a::*, special::UNICODE_LONGFORMS};

pub struct Transformations {
    pub zero_add: bool,                // a + 0 = a and a - 0 = a
    pub zero_mul: bool, // a * 0 = 0 Note: a*(0) does not get transformed to 0, because I'm to lazy to implement it and you can set the `unnecessary_brakets` option
    pub one_mul: bool,  // a * 1 = a
    pub special_func_simple_val: bool, // ln [e] = 1 , ln [1] = 0, ... but not ln [2] = 0.693
    pub unnecessary_brakets: bool, // a (b) = a b
    pub unicode_shorten: bool, // alpha -> α
    pub unicode_lengthen: bool, // α -> alpha
                        //pub local_sum_to_fact: bool, // might convert ab+ac to a(b+c)
}

pub const MY_TRAFOS: Transformations = Transformations {
    zero_add: true,
    zero_mul: true,
    one_mul: true,
    special_func_simple_val: true,
    unnecessary_brakets: true,
    unicode_shorten: true,
    unicode_lengthen: false,
    //local_sum_to_fact: true,
};

impl Symbol {
    fn trafo(&self, trafos: &Transformations) -> Self {
        assert!(!trafos.unicode_shorten || !trafos.unicode_lengthen);
        if trafos.unicode_shorten {
            for (short, long) in UNICODE_LONGFORMS {
                if self.0 == long {
                    return Symbol(short.to_string());
                }
            }
        }
        if trafos.unicode_lengthen {
            for (short, long) in UNICODE_LONGFORMS {
                if self.0 == short {
                    return Symbol(long.to_string());
                }
            }
        }
        self.clone()
    }
}

impl Single {
    fn trafo(&self, trafos: &Transformations) -> Self {
        match self {
            Single::Number(Number::Symbol(x)) => x.trafo(trafos).into(),
            Single::Number(Number::Literal(_)) => self.clone(),
            Single::Braket(s, w1, w2) => Single::Braket(s.trafo(trafos), w1.clone(), w2.clone()),
            Single::Power(bp) => Single::Power(Box::new(Power(
                bp.0.trafo(trafos),
                bp.1.trafo(trafos),
                bp.2.clone(),
            ))),
            Single::Function(f) => Func(
                f.0.trafo(trafos),
                f.1.iter().map(|s| s.trafo(trafos)).collect(),
                f.2.clone(),
            )
            .try_easy_eval(),
            Single::InnerTex(_) => self.clone(),
        }
    }
}

impl Prod {
    fn trafo(&self, trafos: &Transformations) -> Self {
        let zero: Single = 0.into();
        let one: Single = 1.into();
        let mut args = Vec::new();
        let mut spacing = Vec::new();
        for i in 0..self.0.len() {
            let new = self.0[i].trafo(trafos);
            if trafos.zero_mul && new == zero {
                return 0.into();
            }
            if trafos.one_mul && new == one {
                continue;
            }
            if i != 0 {
                spacing.push(self.1[i - 1].clone());
            }
            if trafos.unnecessary_brakets {
                if let Single::Braket(s, _, _) = &new {
                    if s.0.len() == 1
                        && (s.0[0].1 == Sign::ExplicitPlus || s.0[0].1 == Sign::ImplicitPlus)
                    {
                        for factor in &s.0[0].0 .0 {
                            let fac = factor.trafo(trafos);
                            // Style I don't like that the next 6 lines appear two times nearly identical in this function
                            if trafos.zero_mul && fac == zero {
                                return 0.into();
                            }
                            if trafos.one_mul && fac == one {
                                continue;
                            }
                            args.push(fac);
                        }
                        for w in &s.0[0].0 .1 {
                            spacing.push(w.clone());
                        }
                        continue;
                    }
                }
            }
            args.push(new);
        }
        if args.is_empty() {
            one.into()
        } else {
            Prod(args, spacing)
        }
    }
}

impl Sum {
    // fn local_sum_to_fact(&mut self) {
    //     todo!();
    //     let candidates: Vec<Single> = self
    //         .0
    //         .iter()
    //         .map(|(prod, sign)| prod.0.clone())
    //         .flatten()
    //         .collect();
    //     for el in candidates {
    //         let mask: Vec<bool> = self
    //             .0
    //             .iter()
    //             .map(|(prod, sign)| prod.0.contains(&el))
    //             .collect();
    //         if mask.iter().filter(|b| **b).count() == 2 {
    //             dbg!(&el);
    //         }
    //     }
    // }
    pub fn trafo(&self, trafos: &Transformations) -> Self {
        let zero: Prod = 0.into();
        let mut args = Vec::new();
        let mut spacing = Vec::new();
        let mut first_space = true;
        for i in 0..self.0.len() {
            let new = self.0[i].0.trafo(trafos);
            if !trafos.zero_add || new != zero {
                args.push((new, self.0[i].1));
                if !first_space {
                    spacing.push(self.1[2 * i - 1].clone());
                }
                first_space = false;
                spacing.push(self.1[2 * i].clone());
            }
        }
        if args.len() == 1 && spacing.len() == 2 {
            panic!();
        }
        if args.is_empty() {
            zero.into()
        } else {
            Sum(args, spacing)
        }
    }
}

impl Comparison {
    pub fn trafo(&self, trafos: &Transformations) -> Self {
        Comparison {
            cmp_op: self.cmp_op,
            lhs: self.lhs.trafo(trafos),
            rhs: self.rhs.trafo(trafos),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::codegen_eqn::*;
    use crate::pest_parse_eqn::string_to_ast;
    //todo: add a fuzz test that verifies that apply the same transformation twice does not change anything
    #[test]
    fn test_trival_trafos() {
        let pairs = [
            ("a+0+b", "a+b"),
            ("a 1", "a"),
            ("a 0", " 0"),
            ("(a+0+b)", "(a+b)"),
            ("a(0)", " 0"),
        ];
        for (input, output) in pairs {
            let old = string_to_ast(input).unwrap();
            let new = old.trafo(&MY_TRAFOS);
            assert_eq!(new.to_eqn(&CodegenEqnConfig { redo_ws: false }), output);
        }
    }
}
