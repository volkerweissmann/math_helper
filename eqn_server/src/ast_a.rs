use enum_as_inner::EnumAsInner;

/// Used whenever I was to lazy to accurately calculate the whitespace
pub const LAZY_WS: Whitespace = Whitespace(String::new());

pub fn is_equal(a: &Single, b: &Single) -> bool {
    a == b
}

/// A real number
#[derive(Debug, PartialEq, Eq, Clone, Hash)]
#[cfg_attr(feature = "arbitrary", derive(arbitrary::Arbitrary))]
pub struct Symbol(pub String);
impl Symbol {
    pub fn sanity_check(&self) {
        assert!(!self.0.contains(char::is_whitespace));
    }
}

#[derive(Debug, PartialEq, Eq, Clone, EnumAsInner, Hash)]
#[cfg_attr(feature = "arbitrary", derive(arbitrary::Arbitrary))]
pub enum Number {
    Literal(i32),
    Symbol(Symbol),
}
impl Number {
    pub fn sanity_check(&self) {
        match self {
            Number::Symbol(s) => s.sanity_check(),
            Number::Literal(_) => (),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
#[cfg_attr(feature = "arbitrary", derive(arbitrary::Arbitrary))]
pub enum Sign {
    Minus,
    ExplicitPlus,
    ImplicitPlus,
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
#[cfg_attr(feature = "arbitrary", derive(arbitrary::Arbitrary))]
pub struct Whitespace(pub String); // todo performance: maybe use &str instead of String
impl Default for Whitespace {
    fn default() -> Self {
        Whitespace(String::from(" "))
    }
}
impl Whitespace {
    pub fn sanity_check(&self) {
        assert!(self.0.trim().is_empty());
    }
}

// #[derive(Debug, PartialEq, Eq, Clone)]
// #[cfg_attr(feature = "arbitrary", derive(arbitrary::Arbitrary))]
// pub struct Term(pub Sum, pub Whitespace, pub Whitespace);

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
#[cfg_attr(feature = "arbitrary", derive(arbitrary::Arbitrary))]
pub struct Sum(pub Vec<(Prod, Sign)>, pub Vec<Whitespace>);
impl Sum {
    /// constructs a Sum if you are to lazy to calculate the correct whitespaces
    pub fn lazy_ws(summands: Vec<(Prod, Sign)>) -> Self {
        let len = summands.len();
        Sum(summands, vec![Whitespace::default(); len * 2 - 1])
    }
    /// constructs a Sum if you are to lazy to calculate the correct whitespaces
    pub fn lazy_ws_plus(summands: Vec<Prod>) -> Self {
        let len = summands.len();
        let mut summands: Vec<_> = summands
            .into_iter()
            .map(|p| (p, Sign::ExplicitPlus))
            .collect();
        summands[0].1 = Sign::ImplicitPlus;
        Sum(summands, vec![Whitespace::default(); len * 2 - 1])
    }
    pub fn sanity_check(&self) {
        for i in 1..self.0.len() {
            assert!(self.0[i].1 != Sign::ImplicitPlus);
        }
        assert_eq!(self.1.len(), self.0.len() * 2 - 1);
        self.0.iter().for_each(|el| el.0.sanity_check());
        self.1.iter().for_each(|el| el.sanity_check());
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
#[cfg_attr(feature = "arbitrary", derive(arbitrary::Arbitrary))]
pub struct Prod(pub Vec<Single>, pub Vec<Whitespace>);
impl Prod {
    pub fn lazy_ws(facs: Vec<Single>) -> Self {
        let len = facs.len();
        Prod(facs, vec![Whitespace::default(); len - 1])
    }
    pub fn sanity_check(&self) {
        assert_eq!(self.1.len(), self.0.len() - 1);
        self.0.iter().for_each(|el| el.sanity_check());
        self.1.iter().for_each(|el| el.sanity_check());
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
#[cfg_attr(feature = "arbitrary", derive(arbitrary::Arbitrary))]
pub enum PowerFormat {
    Normal(Whitespace, Whitespace), // b^c
    Div(Whitespace),                // a/b gets parsed as a b^-1
}
impl PowerFormat {
    pub fn sanity_check(&self) {
        match self {
            PowerFormat::Normal(w1, w2) => {
                w1.sanity_check();
                w2.sanity_check();
            }
            PowerFormat::Div(w) => w.sanity_check(),
        }
    }
}

/// self.0 ^ self.1
#[derive(Debug, PartialEq, Eq, Clone, Hash)]
#[cfg_attr(feature = "arbitrary", derive(arbitrary::Arbitrary))]
pub struct Power(pub Single, pub Single, pub PowerFormat);
impl Power {
    pub fn sanity_check(&self) {
        if let PowerFormat::Div(_) = &self.2 {
            assert_eq!(self.1, Single::Number(Number::Literal(-1)));
        }
        self.0.sanity_check();
        self.1.sanity_check();
        self.2.sanity_check();
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
#[cfg_attr(feature = "arbitrary", derive(arbitrary::Arbitrary))]
pub struct Func(pub Symbol, pub Vec<Sum>, pub Vec<Whitespace>);
impl Func {
    pub fn lazy_ws(func: Symbol, args: Vec<Sum>) -> Self {
        let len = args.len();
        Func(func, args, vec![Whitespace::default(); len * 2 + 1])
    }
    pub fn sanity_check(&self) {
        assert_eq!(self.2.len(), self.1.len() * 2 + 1);
        self.0.sanity_check();
        self.1.iter().for_each(|el| el.sanity_check());
        self.2.iter().for_each(|el| el.sanity_check());
    }
}

#[derive(Debug, PartialEq, Eq, Clone, EnumAsInner, Hash)]
#[cfg_attr(feature = "arbitrary", derive(arbitrary::Arbitrary))]
pub enum Single {
    Number(Number),
    Braket(Sum, Whitespace, Whitespace),
    Power(Box<Power>),
    Function(Func),
    InnerTex(String),
}
impl Single {
    pub fn sanity_check(&self) {
        match self {
            Single::Number(x) => x.sanity_check(),
            Single::Braket(s, w1, w2) => {
                s.sanity_check();
                w1.sanity_check();
                w2.sanity_check();
            }
            Single::Power(bp) => {
                bp.sanity_check();
            }
            Single::Function(f) => f.sanity_check(),
            Single::InnerTex(_) => (),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
#[cfg_attr(feature = "arbitrary", derive(arbitrary::Arbitrary))]
pub enum CmpOp {
    Equal,
    Definition,
    Lesser,
    LesserEqual,
    Greater,
    GreaterEqual,
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
#[cfg_attr(feature = "arbitrary", derive(arbitrary::Arbitrary))]
pub struct Comparison {
    pub cmp_op: CmpOp,
    pub lhs: Sum,
    pub rhs: Sum,
}

// pub enum Term {
//     Number(Number),
//     Sum(Vec<Term, Sign>),
//     Prod(Vec<Term>),
// }

impl From<i32> for Number {
    fn from(x: i32) -> Self {
        Number::Literal(x)
    }
}
impl From<Prod> for Sum {
    fn from(prod: Prod) -> Self {
        Self(
            vec![(prod, Sign::ImplicitPlus)],
            vec![Whitespace::default()],
        )
    }
}
impl From<Sum> for Prod {
    fn from(sum: Sum) -> Self {
        Single::Braket(sum, Whitespace::default(), Whitespace::default()).into()
    }
}
impl From<Symbol> for Sum {
    fn from(x: Symbol) -> Self {
        let x1: Number = x.into();
        x1.into()
    }
}
impl From<Number> for Sum {
    fn from(x: Number) -> Self {
        let x1: Prod = x.into();
        x1.into()
    }
}
impl From<Symbol> for Number {
    fn from(x: Symbol) -> Self {
        Number::Symbol(x)
    }
}
impl From<Single> for Sum {
    fn from(x: Single) -> Self {
        let x1: Prod = x.into();
        x1.into()
    }
}
impl From<Func> for Sum {
    fn from(x: Func) -> Self {
        let x2: Single = x.into();
        let x1: Prod = x2.into();
        x1.into()
    }
}
impl From<i32> for Sum {
    fn from(x: i32) -> Self {
        let x1: Prod = x.into();
        x1.into()
    }
}
impl From<i32> for Single {
    fn from(x: i32) -> Self {
        let x1: Number = x.into();
        x1.into()
    }
}
impl From<Single> for Prod {
    fn from(single: Single) -> Self {
        Self(vec![single], vec![])
    }
}
impl From<Number> for Prod {
    fn from(x: Number) -> Self {
        let x1: Single = x.into();
        x1.into()
    }
}
impl From<i32> for Prod {
    fn from(x: i32) -> Self {
        let x1: Number = x.into();
        x1.into()
    }
}
impl From<Symbol> for Single {
    fn from(x: Symbol) -> Self {
        Single::Number(x.into())
    }
}
impl From<Number> for Single {
    fn from(x: Number) -> Self {
        Single::Number(x)
    }
}
impl From<Power> for Single {
    fn from(x: Power) -> Self {
        Single::Power(Box::new(x))
    }
}
impl From<Box<Power>> for Single {
    fn from(x: Box<Power>) -> Self {
        Single::Power(x)
    }
}
impl From<Func> for Single {
    fn from(x: Func) -> Self {
        Single::Function(x)
    }
}
