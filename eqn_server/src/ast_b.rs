use enum_as_inner::EnumAsInner;
use std::cell::Ref;
use std::cell::RefCell;
use std::rc::Rc;

#[derive(Debug, EnumAsInner)]
pub enum Inner {
    Literal(i32),
    Variable(String),
    Function(String, Vec<Rc<RefCell<Inner>>>),
    Sum(Vec<Rc<RefCell<Inner>>>),
    Prod(Vec<Rc<RefCell<Inner>>>),
    Power(Rc<RefCell<Inner>>, Rc<RefCell<Inner>>),
}

#[derive(PartialEq, Eq)]
pub enum InnerVariant {
    Literal,
    Variable,
    Function,
    Sum,
    Prod,
    Power,
}

impl Inner {
    //todo: isn't there some [derive(...)] that can codegen this function? Or some language feature that can replace it?
    pub fn get_variant(&self) -> InnerVariant {
        match self {
            Inner::Literal(_) => InnerVariant::Literal,
            Inner::Variable(_) => InnerVariant::Variable,
            Inner::Function(_, _) => InnerVariant::Function,
            Inner::Sum(_) => InnerVariant::Sum,
            Inner::Prod(_) => InnerVariant::Prod,
            Inner::Power(_, _) => InnerVariant::Power,
        }
    }
    pub fn is_integer(&self) -> bool {
        matches!(self, Inner::Literal(_))
    }
    pub fn try_integer(&self) -> Option<i32> {
        if let Inner::Literal(i) = self {
            Some(*i)
        } else {
            None
        }
    }
}

// pub struct Factors<'a>(std::slice::Iter<'a, Rc<Node>>);
// impl<'a> Iterator for Factors<'a> {
//     type Item = &'a Rc<Node>;
//     fn next(&mut self) -> Option<Self::Item> {
//         // todo performance: Check if we can do this iterator with one less pointer indirection
//         self.0.next()
//     }
// }

// pub struct Factors<'a>(std::slice::Iter<'a, Rc<Node>>);
// impl<'a> Iterator for Factors<'a> {
//     type Item = &'a Rc<Node>;
//     fn next(&mut self) -> Option<Self::Item> {
//         // todo performance: Check if we can do this iterator with one less pointer indirection
//         self.0.next()
//     }
// }

// pub struct Factors(Vec<Rc<Node>>, usize);
// // todo performance: Is there a better way to do this
// impl Iterator for Factors {
//     type Item = Rc<Node>;
//     fn next(&mut self) -> Option<Self::Item> {
//         if self.1 >= self.0.len() {
//             None
//         } else {
//             self.1 += 1;
//             Some(Rc::clone(&self.0[self.1 - 1]))
//         }
//     }
// }
// impl ExactSizeIterator for Factors {
//     fn len(&self) -> usize {
//         self.0.len()
//     }
//     // fn is_empty(&self) -> bool {
//     //     self.0.is_empty()
//     // }
// }

// pub struct Factors(pub Vec<Rc<Node>>);
#[allow(dead_code)]
pub fn gimme_factors(node: Rc<RefCell<Inner>>) -> Vec<Rc<RefCell<Inner>>> {
    match &*node.borrow() {
        Inner::Prod(x) => x.to_vec(),
        _ => vec![node.clone()],
    }
}
#[allow(dead_code)]
pub fn gimme_multiple_factors(node: Rc<RefCell<Inner>>) -> Option<Vec<Rc<RefCell<Inner>>>> {
    match &*node.borrow() {
        Inner::Prod(x) => Some(x.to_vec()),
        _ => None,
    }
}

pub fn select<B, F>(vec: Vec<Rc<RefCell<Inner>>>, cond: F) -> (Vec<usize>, Vec<B>)
where
    F: Fn(Ref<Inner>) -> Option<B>,
{
    let temp: Vec<_> = vec.into_iter().map(|x| cond(x.borrow())).collect();

    let indexes: Vec<_> = temp
        .iter()
        .enumerate()
        .filter_map(|(u, x)| if x.is_some() { Some(u) } else { None })
        .collect();

    let interesting: Vec<_> = temp.into_iter().flatten().collect();
    (indexes, interesting)
}

pub fn flatten_single_arg(node: Rc<RefCell<Inner>>) {
    // let variant = node.borrow().get_variant();
    // if variant == InnerVariant::Prod {
    //     let facs = &*node.borrow().as_prod().unwrap()
    //     // let facs = match &*node.borrow() {
    //     //     Inner::Prod(x) => x.to_vec(),
    //     //     _ => return,
    //     // };
    // } else if variant == InnerVariant::Sum {
    // }

    let mut new = None;
    match &*node.borrow() {
        Inner::Literal(_) => (),
        Inner::Variable(_) => (),
        Inner::Function(_, _) => (),
        Inner::Power(_, _) => (),
        Inner::Prod(vec) => {
            if vec.len() == 1 {
                new = Some(vec[0].clone());
            }
        }
        Inner::Sum(vec) => {
            if vec.len() == 1 {
                new = Some(vec[0].clone());
            }
        }
    }
    if let Some(new) = new {
        node.swap(&new);
    }
}

pub struct Replacement {
    pub remove: Vec<usize>,
    pub additional: Vec<Inner>,
}

pub fn apply_factor_trafo<F>(node: Rc<RefCell<Inner>>, logic: F)
where
    F: Fn(Vec<Rc<RefCell<Inner>>>) -> Option<Replacement>,
{
    let facs = match &*node.borrow() {
        Inner::Prod(x) => x.to_vec(),
        _ => return,
    };
    if let Some(command) = logic(facs.clone()) {
        let mut new: Vec<_> = facs
            .iter()
            .enumerate()
            .filter(|(u, _x)| !command.remove.contains(u))
            .map(|(_u, x)| x.clone())
            .collect();
        let mut additional: Vec<_> = command
            .additional
            .into_iter()
            .map(|x| Rc::new(RefCell::new(x)))
            .collect();
        new.append(&mut additional);
        node.replace(Inner::Prod(new));
        //command.indexes
    }
}

pub fn apply_summand_trafo<F>(node: Rc<RefCell<Inner>>, logic: F)
where
    F: Fn(Vec<Rc<RefCell<Inner>>>) -> Option<Replacement>,
{
    let summands = match &*node.borrow() {
        Inner::Sum(x) => x.to_vec(),
        _ => return,
    };
    if let Some(command) = logic(summands.clone()) {
        let mut new: Vec<_> = summands
            .iter()
            .enumerate()
            .filter(|(u, _x)| !command.remove.contains(u))
            .map(|(_u, x)| x.clone())
            .collect();
        let mut additional: Vec<_> = command
            .additional
            .into_iter()
            .map(|x| Rc::new(RefCell::new(x)))
            .collect();
        new.append(&mut additional);
        node.replace(Inner::Sum(new));
        //command.indexes
    }
}
