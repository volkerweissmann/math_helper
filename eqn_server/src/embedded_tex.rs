extern crate gag;

use math_helper::codegen_latex::*;
use std::env;
use std::fs::read_to_string;
use std::fs::File;
use std::io::prelude::*;

fn convert_file(partial_path: &str) {
    let mut conf = TexConfig::default();
    conf.sym_confs.insert(
        "q".to_string(),
        SymConfig {
            bold: true,
            hat: false,
        },
    );
    conf.sym_confs.insert(
        "zeta".to_string(),
        SymConfig {
            bold: true,
            hat: false,
        },
    );
    conf.sym_confs.insert(
        "C".to_string(),
        SymConfig {
            bold: false,
            hat: true,
        },
    );
    conf.sym_confs.insert(
        "L_q".to_string(),
        SymConfig {
            bold: false,
            hat: true,
        },
    );
    conf.sym_confs.insert(
        "L_x".to_string(),
        SymConfig {
            bold: false,
            hat: true,
        },
    );
    let input = read_to_string(format!("{}.tex", partial_path)).unwrap();
    let output = match math_helper::embedding::latex_with_eqn_to_latex(&input, &conf) {
        Ok(v) => v,
        Err(e) => panic!("{}", e),
    };
    std::fs::write(format!("{}.myout", partial_path), output).expect("unable to write");
}

fn main() {
    let log = std::fs::OpenOptions::new()
        .truncate(true)
        .read(true)
        .create(true)
        .write(true)
        .open("foo.txt")
        .unwrap();

    //let print_redirect = gag::Redirect::stdout(log).unwrap();
    //let print_redirect = gag::Redirect::stderr(log).unwrap();

    let args: Vec<String> = env::args().collect();
    dbg!(&args);

    let path = &args[1];
    // if !path.starts_with("/home/volker/Sync/DatenVolker/git/Masterarbeit/") {
    //     return;
    // }
    convert_file(&path[..path.len() - 4]);
}
