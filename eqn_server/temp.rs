
// // note that Inner does not impl Copy
// #[derive(Debug, Clone)]
// struct Inner(i32);

// struct Counter {
//     data: Vec<Inner>,
//     count: usize,
// }

// impl Counter {
//     fn new() -> Counter {
//         Counter {
//             data: vec![Inner(10), Inner(11), Inner(12)],
//             count: 0,
//         }
//     }
// }
// impl Iterator for Counter {
//     type Item = Inner;
//     fn next(&mut self) -> Option<Self::Item> {
//         self.count += 1;
//         if self.count < 3 {
//             Some(self.data[self.count - 1].clone())
//         } else {
//             None
//         }
//     }
// }





use std::rc::Rc;

#[derive(Debug)]
struct Inner(String);

fn temp() {
    // Note that Inner does not implement Copy

    // let x = Inner(123);
    // let vec = vec![x, x]; // compile time error

    // let x = Box::new(Inner(123));
    // let vec = vec![x, x]; // compile time error

    let mut x = Rc::new(Inner(String::new()));
    let vec = vec![x.clone(), x.clone()];

    dbg!((*vec[0]).0.chars());
    // pub fn downgrade(this: &Rc<T>) -> Weak<T>
    // pub fn get_mut(this: &mut Rc<T>) -> Option<&mut T>
    // pub fn ptr_eq(this: &Rc<T>, other: &Rc<T>) -> bool
    // pub fn make_mut(this: &mut Rc<T>) -> &mut T
    // pub fn try_unwrap(this: Rc<T>) -> Result<T, Rc<T>>

    // pub fn upgrade(&self) -> Option<Rc<T>>
    // pub fn ptr_eq(&self, other: &Weak<T>) -> bool
}


pub fn literal_addition(self) -> Self {
    let sum: i32 = self
        .0
        .iter()
        .map(|(prod, sign)| -> i32 {
            if prod.0.len() == 1 {
                if let Single::Number(Number::Literal(x)) = &prod.0[0] {
                    return match sign {
                        Sign::ImplicitPlus => *x,
                        Sign::ExplicitPlus => *x,
                        Sign::Minus => -(*x),
                    };
                }
            }
            0
        })
        .sum();

    let mut vec: Vec<_> = self
        .0
        .into_iter()
        .filter(|(prod, _sign)| {
            if prod.0.len() == 1 {
                if let Single::Number(Number::Literal(_)) = &prod.0[0] {
                    return false;
                }
            }
            true
        })
        .collect();
    if sum != 0 {
        vec.push((sum.into(), Sign::ExplicitPlus));
    }
    Sum::lazy_ws(vec)
}

fn mul_to_exp_sum(self, fac: &Single) {
    let vec = prod_to_iter_base_expr(self).filter_map -> Option<Prod> (
        |base, exp| {
            if is_equal(base, fac) {
                Some(exp.into())
            } else {
                None
            }
        }
    ).collect();

    if vec.len() == 1 {
        return;
    }


    let exponent = if exponent.0.len() == 1 && exponent.0[0].0 .0.len() == 1 {
        exponent.0[0].0 .0[0].clone()
    } else {
        Single::Braket(exponent, LAZY_WS, LAZY_WS)
    };
    if exponent != 1.into() {
        factors.push(Single::Power(Box::new(Power(
            fac.clone(),
            exponent,
            PowerFormat::Normal(LAZY_WS, LAZY_WS),
        ))));
    } else {
        factors.push(fac.clone());
    }

    Prod::lazy_ws(factors)
}

fn mul_to_exp_sum(self, fac: &Single) -> Self {
    if self.0.len() == 1 {
        // Without this short-circuit evaluation, we got infinite recursion
        return self;
    }
    let exponents: Vec<_> = self
        .0
        .iter()
        .filter_map(|x| -> Option<Prod> {
            let (base, exp) = x.get_base_and_exponent();
            if is_equal(base, fac) {
                Some(exp.into())
            } else {
                None
            }
        })
        .collect();
    let exponent = Sum::lazy_ws_plus(exponents).simplify();

    let mut factors: Vec<_> = self
        .0
        .into_iter()
        .filter(|x| -> bool {
            if let Some(bp) = x.as_power() {
                if is_equal(&bp.0, fac) {
                    false
                } else {
                    true
                }
            } else if is_equal(x, fac) {
                false
            } else {
                true
            }
        })
        .collect();

    let exponent = if exponent.0.len() == 1 && exponent.0[0].0 .0.len() == 1 {
        exponent.0[0].0 .0[0].clone()
    } else {
        Single::Braket(exponent, LAZY_WS, LAZY_WS)
    };
    if exponent != 1.into() {
        factors.push(Single::Power(Box::new(Power(
            fac.clone(),
            exponent,
            PowerFormat::Normal(LAZY_WS, LAZY_WS),
        ))));
    } else {
        factors.push(fac.clone());
    }

    Prod::lazy_ws(factors)

    // todo: do something here
}
