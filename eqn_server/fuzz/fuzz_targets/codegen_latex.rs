#![no_main]
use libfuzzer_sys::fuzz_target;

extern crate math_helper;

fuzz_target!(|ast: math_helper::ast::Sum| {
    ast.to_latex();
});
