#![no_main]
use libfuzzer_sys::fuzz_target;

fuzz_target!(|ast: math_helper::ast::Sum| {
    let eqn = ast.to_eqn();
    let total = math_helper::pest_parse_eqn::export_eqn_parse(
        math_helper::pest_parse_eqn::ExportRule::export,
        &eqn,
    )
    .unwrap();
    let term = math_helper::pest_parse_eqn::parse_total(total).unwrap();
    assert_eq!(ast, term);
});
