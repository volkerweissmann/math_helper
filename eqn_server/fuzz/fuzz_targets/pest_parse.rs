#![no_main]
use libfuzzer_sys::fuzz_target;

extern crate math_helper;

fuzz_target!(|data: &[u8]| {
    if let Ok(s) = std::str::from_utf8(data) {
        if s.matches("(").count() < 1000 {
            // To many opening brakets can make pest overflow its stack
            if let Ok(total) = math_helper::pest_parse_eqn::export_eqn_parse(
                math_helper::pest_parse_eqn::ExportRule::export,
                s,
            ) {
                let term = math_helper::pest_parse_eqn::parse_total(total);
                if let Ok(v) = term {
                    v.sanity_check();
                }
            }
        }
    }
});
