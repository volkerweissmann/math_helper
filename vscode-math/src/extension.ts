// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "vscode-math" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable1 = vscode.commands.registerCommand('vscode-math.latexToClipboard', () => {
		// The code you place here will be executed every time your command is executed
		// Display a message box to the user
		var editor = vscode.window.activeTextEditor;
		if (!editor) {
			return; // No open text editor
		}
		var selection = editor.selection;
		var start = selection.start;
		var end = selection.end;

		var net = require('net');

		//vscode.env.clipboard.writeText

		var client = new net.Socket();
		client.connect(65432, '127.0.0.1', function () {
			console.log('Connected');
			client.write(JSON.stringify({ cmd: "latexToClipboard", start_line: start.line, start_column: start.character, end_line: end.line, end_column: end.character, editor_text: editor?.document.getText() }) + "\n");
		});
		client.on('data', function (data: any) {
			console.log('Received: ' + data);
			vscode.env.clipboard.writeText(String(data));
			client.destroy(); // kill client after server's response
		});

		client.on('close', function () {
			console.log('Connection closed');
		});


		// var text = editor.document.getText(new vscode.Range(a, b));

		//vscode.window.showInformationMessage('Hello World from vscode-math!' + String(a.compareTo(new vscode.Position(0, 0))) + " " + String(typeof (b)) + "  " + String(text));
	});

	let disposable2 = vscode.commands.registerCommand('vscode-math.expandOuterFunction', () => {
		// The code you place here will be executed every time your command is executed
		// Display a message box to the user
		var editor = vscode.window.activeTextEditor;
		if (!editor) {
			return; // No open text editor
		}
		var selection = editor.selection;
		var start = selection.start;
		var end = selection.end;

		var net = require('net');

		//vscode.env.clipboard.writeText

		var client = new net.Socket();
		client.connect(65432, '127.0.0.1', function () {
			console.log('Connected');
			client.write(JSON.stringify({ cmd: "expandOuterFunction", start_line: start.line, start_column: start.character, end_line: end.line, end_column: end.character, editor_text: editor?.document.getText() }) + "\n");
		});
		client.on('data', function (data: any) {
			console.log('Received: ' + data);
			vscode.env.clipboard.writeText(String(data));
			client.destroy(); // kill client after server's response
		});

		client.on('close', function () {
			console.log('Connection closed');
		});


		// var text = editor.document.getText(new vscode.Range(a, b));

		//vscode.window.showInformationMessage('Hello World from vscode-math!' + String(a.compareTo(new vscode.Position(0, 0))) + " " + String(typeof (b)) + "  " + String(text));
	});

	context.subscriptions.push(disposable1);
	context.subscriptions.push(disposable2);
}

// this method is called when your extension is deactivated
export function deactivate() { }
